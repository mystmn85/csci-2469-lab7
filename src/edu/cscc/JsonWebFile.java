package edu.cscc;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

public class JsonWebFile {

	private static String apiCall;
	
	public static void readingFile() throws IOException {
       BufferedReader file = new BufferedReader(new InputStreamReader(HttpConnection.getConnectionMessage().getInputStream()));
       setStoreContentOfFile(file.readLine());
       file.close();
   }

	public static String getStoreContentOfFile() {
		return apiCall;
	}

	private static void setStoreContentOfFile(String apiCall) {
		JsonWebFile.apiCall = apiCall;
	}
}
