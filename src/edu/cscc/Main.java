package edu.cscc;

import java.io.IOException;
import java.util.ArrayList;

import org.json.*;
/**
 * Find out who's in space JSON library from:
 * https://github.com/stleary/JSON-java
 * 
 * @author Paul Cameron
 * 03/04/2022
 * Lab07
 * 			Purpose: Application makes a call to a url API, retrieves the data as a JSON, 
 * 			parses the objects, and System prints two String fields called name and craft
 */
public class Main {

	public static void main(String[] args) throws IOException {
		HttpConnection.connect();
		JsonWebFile.readingFile();
		JsonWebFile.getStoreContentOfFile();
		ParseDataOfFile.parseAndPrint();
	}
}

