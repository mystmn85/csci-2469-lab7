package edu.cscc;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

public class HttpConnection {
		
	private static HttpURLConnection connection;
	
	public static void connect() {
		try {
			URL url = new URL(UrlPath.theURLString);
			connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
//            getResponse(connection);
//        	line();

		} catch(IOException e) {
			System.out.println(String.format("Received :: %s", e));
		}
	}
	public static URLConnection getConnectionMessage() {
		return connection;
	}
	
//	protected static void getResponse(HttpURLConnection connection) throws IOException {
//        System.out.println("Reponse code: " + connection.getResponseCode()+
//            " Response message: " + connection.getResponseMessage());
//    }
//	 
//	protected static void line() {
//    	// Thank you professor for showing me .repeat
//        System.out.println("-".repeat(20));
//    }
}
