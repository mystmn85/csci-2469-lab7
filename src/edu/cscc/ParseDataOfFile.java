package edu.cscc;

import org.json.*;

public class ParseDataOfFile {
	private static JSONObject jsonObj = new JSONObject(JsonWebFile.getStoreContentOfFile());
//	protected static String name; 
//	protected static String craft;
	private static final String parentObj = "people";

	public static void parseAndPrint() {
		JSONArray ja_data = jsonObj.getJSONArray(parentObj);
		int length = ja_data.length();
		
		System.out.print(String.format("There are %s people in space \n", length));

		for(int i=0; i < length; i++) {
		    JSONObject jObj = ja_data.getJSONObject(i);
//		    craft = jObj.getString("craft");
//		    name = jObj.getString("name");

		    System.out.println(String.format("%s %s %s", jObj.getString("name"), Messages.ONBOARD, jObj.getString("craft")));
		}
	}
}
